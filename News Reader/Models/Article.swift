//
//  Article.swift
//  News Reader
//
//  Created by Mykola Hrynchuk on 8/19/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import Foundation

struct News: Decodable {
    let articles: [ArticleData]?
}

struct ArticleData: Codable {
    let source: Source?
    let author: String?
    let title, description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
    
}

struct Source: Codable {
    let id: String?
    let name: String?
}

