//
//  ViewController.swift
//  News Reader
//
//  Created by Mykola Hrynchuk on 8/19/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import UIKit
import SafariServices


class ViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var currentPage = 1
    var resultArray = [ArticleData]()
    var newsUrl = "https://newsapi.org/v2/everything?sources=the-verge&page=1&apiKey=b346a5f8790b467e8110a7f9926582e7"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData(newsUrl) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        searchBar.delegate = self
        setRefreshControl()
    }
    
    private func fetchData(_ url: String, completion: (() -> Void)?) {
        NewsDataService.shared.downloadFromJson(url: url) { data in
            if let data = data {
                self.resultArray += data
                completion?()
            }
        }
    }
    
    private func setRefreshControl() {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(reshreshData(sender:)), for: .valueChanged)
        tableView.addSubview(refresher)
    }
    
    @objc func reshreshData(sender: UIRefreshControl) {
        if sender.isRefreshing {
            fetchData(newsUrl) {
                DispatchQueue.main.async {
                    sender.endRefreshing()
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func showWeb(url: String) {
        let webURL = URL(string: url)!
        let webVC = SFSafariViewController(url: webURL)
        present(webVC, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchBar = searchBar.text {
            let searchKey = "q=" + searchBar + "&"
            newsUrl = "https://newsapi.org/v2/everything?" + searchKey + "apiKey=b346a5f8790b467e8110a7f9926582e7"
            resultArray = []
            fetchData(newsUrl) {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        searchBar.endEditing(true)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == resultArray.count - 1 {
            currentPage += 1
            let pagination = "page=" + String(currentPage) + "&"
            newsUrl = "https://newsapi.org/v2/everything?sources=the-verge&" + pagination + "apiKey=b346a5f8790b467e8110a7f9926582e7"
            
            fetchData(newsUrl) {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell") as? ArticleCell
            else {
                return UITableViewCell()
        }
        
        if let sources = resultArray[indexPath.row].source?.name {
            cell.source.text = "From: " + sources
        } else { cell.source.text = "Source is unknown" }
        
        if let authors = resultArray[indexPath.row].author {
            cell.author.text = "Author: " + authors}
        else { cell.author.text = "Author is unknown" }
        
        if let titles = resultArray[indexPath.row].title {
            cell.title.text = titles}
        else { cell.title.text = "Title: N/A" }
        
        if let descriptions = resultArray[indexPath.row].description {
            cell.descriptoin.text = descriptions}
        else { cell.descriptoin.text = "Click to read more..." }
        
        if let url = resultArray[indexPath.row].urlToImage {
            if let imageURL = URL(string: url) {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: imageURL)
                    if let data = data {
                        let imageFromUrl = UIImage(data: data)
                        DispatchQueue.main.async {
                            cell.img.image = imageFromUrl
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellData = resultArray[indexPath.row]
        if let url = cellData.url {
            self.showWeb(url: url)
        }
    }
}
