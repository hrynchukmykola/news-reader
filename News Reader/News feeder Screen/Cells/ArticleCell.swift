//
//  ArticleCell.swift
//  News Reader
//
//  Created by Mykola Hrynchuk on 8/19/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {
   
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptoin: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var source: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()

       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
