//
//  NewsDataService.swift
//  News Reader
//
//  Created by Mykola Hrynchuk on 8/26/19.
//  Copyright © 2019 Mykola Hrynchuk. All rights reserved.
//

import Foundation

class NewsDataService {
    static let shared = NewsDataService()
    
    func downloadFromJson(url: String,
                                  competionHandler: @escaping ([ArticleData]?) -> Void) {
        let jsonUrlString = URL(string: url)
        if let url = jsonUrlString {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard let data = data
                    else { return }
                do {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let downloadedArticles =  try decoder.decode(News.self, from: data)
                    if let downloadedArticles = downloadedArticles.articles {
                        competionHandler(downloadedArticles)
                    }
                } catch let jsonError {
                    print("JSON error :", jsonError)
                }
                }.resume()
        }
    }
}
